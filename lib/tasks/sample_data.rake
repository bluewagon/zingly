namespace :db do
  desc 'Fill database with sample data'
  task populate: :environment do
    make_users
    make_groups
    add_users_to_groups
    add_points
  end
end


def make_users
  User.create!(username: 'bluewagon', email: 'test@example.com', password: 'foobar123', password_confirmation: 'foobar123')

  30.times do |n|
    username = Faker::Internet.user_name
    email = Faker::Internet.email
    password = 'password'
    User.create!(username: username, email: email, password: password, password_confirmation: password)
  end

end

def make_groups
  user1 = User.first
  user2 = User.find(15)
  Group.create!(name: 'The almighty Group', description: 'Test description', admin: user1)
  Group.create!(name: 'Alpha Bogus', description: 'This aint no desc', admin: user2)
end

def add_users_to_groups
  users = User.all
  group1 = Group.first
  group2 = Group.find(2)

  group1.users << users[0..15]
  group2.users << users[10..30]

  group1.save!
  group2.save!
end

def add_points
  group1 = Group.first
  group2 = Group.find(2)

  group1_user_count = group1.users.count
  group2_user_count = group2.users.count

  30.times do |n|
    user1 = group1.users.find(rand(group1_user_count)+1)
    user2 = group1.users.find(rand(group1_user_count)+1)

    while user2 == user1
      user2 = group1.users.find(rand(group1_user_count)+1)
    end

    user1_index = group1.rankings.index { |a| a.user_id == user1.id }
    user2_index = group1.rankings.index { |a| a.user_id == user2.id }
    zing = group1.zings.build(zing_for: user1, zing_against: user2, creator: user1, description: 'Testers')

    group1.rankings[user1_index].points += 1
    group1.rankings[user1_index].zings_for += 1
    group1.rankings[user1_index].last_position = user1_index + 1


    group1.rankings[user2_index].points -= 1
    group1.rankings[user2_index].zings_against += 1
    group1.rankings[user2_index].last_position = user2_index + 1

    group1.rankings[user1_index].save!
    group1.rankings[user2_index].save!
    zing.save!
  end
end