require 'spec_helper'

describe 'User Pages' do
  let(:user) { FactoryGirl.create(:user) }

  subject { page }

  describe 'when not logged in' do
    before { visit root_path }
    it { should have_button('Sign in') }
  end

  describe 'User Home Page' do
    before do
      visit root_path
      sign_in user
    end

    it { should have_link('Sign out', href: destroy_user_session_path) }
    it { should have_link('Edit', href: edit_user_registration_path) }
    it { should have_link('New Group', href: new_group_path) }
    it { should have_css('img.gravatar') }

    it { should have_text(user.email) }

    describe 'with username' do
      before do
        user.username = 'bluewagon'
        user.save!
        visit root_path  
      end
      
      it { should have_text(user.username) }
    end

    describe 'with group as admin' do
      before do
        @group = Group.new(name: 'test', description: 'desc', admin: user)
        @group.users << user
        @group.save!
        visit root_path
      end

      it { should have_link(@group.name) }
      #it { should have_text("admin") }
    end

    describe 'with group as user' do
      let(:admin_user) { FactoryGirl.create(:user) }
      before do
        @group = Group.new(name:'TesT', description: 'desc', admin: admin_user)
        @group.users << admin_user
        @group.users << user
        @group.save!
        visit root_path
      end

      it { should have_link(@group.name) }
      #it { should_not have_text("admin") }
    end
  end

  describe 'Edit user profile' do
    before do
      visit root_path
      sign_in user
      visit edit_user_registration_path
    end

    it { should have_text('Edit User') }

    it 'should have input email with user email in value' do
      expect { find_field('user_email').value.should == user.email }
    end

    it 'should have username input' do
      expect { find_field('user_username').value.should == '' }
    end

    it { should have_field('user_password') }
    it { should have_field('user_password_confirmation') }
    it { should have_field('user_current_password') }
    it { should have_button('Update') }
    it { should have_button('Cancel my account') }

    describe 'with username' do
      before do
        user.username = 'my_username'
        user.save!
      end

      it "should have user's username in input" do
        expect { find_field('user_username').value.should == user.username }
      end
    end

    describe 'updating without entering password' do
        before { click_button('Update') }

        it { should have_css('#error_explanation') }
    end

    describe 'when updating email' do

      describe 'with an email already taken' do
        let(:other_user) { FactoryGirl.create(:user) }
        before do
          fill_in 'user_email', with: other_user.email
          fill_in 'user_current_password', with: user.password
          click_button('Update')
        end

        it { should have_css('#error_explanation') }
      end

      describe 'with an invalid email' do
        before do
          fill_in 'user_email', with: nil
          fill_in 'user_current_password', with: user.password
          click_button('Update')
        end

        it { should have_css('#error_explanation') }
      end

      describe 'with valid email' do
        before do
          visit edit_user_registration_path
          fill_in 'user_email', with: 'HolyMoleyBatman@example.com'
          fill_in 'user_username', with: nil
          fill_in 'user_current_password', with: user.password
          click_button('Update')
        end

        it { should have_text('HolyMoleyBatman@example.com'.downcase!) }
        

        it 'should redirect to user home page' do
          expect { current_url == root_path }
        end
      end
    end

    describe 'when updating username' do
      describe 'from empty' do
        before do
          user.username = nil
          user.save!
          visit edit_user_registration_path
          fill_in 'user_username', with: 'bluewagon'
          fill_in 'user_current_password', with: user.password
          click_button 'Update'
        end

        it { should have_text('bluewagon') }
      end

      describe 'to empty' do
        before do
          user.username = 'bluewagon'
          user.save!
          visit edit_user_registration_path

          fill_in 'user_username', with: nil
          fill_in 'user_current_password', with: user.password
          click_button 'Update'
        end

        it { should have_text(user.email) }
      end

      describe 'with not enough characters' do
        before do
          fill_in 'user_username', with: 'a'
          fill_in 'user_current_password', with: user.password
          click_button 'Update'
        end

        it { should have_css('#error_explanation') }
      end

      describe 'with too many characters' do
        before do
          fill_in 'user_username', with: 'a' * 51
          fill_in 'user_current_password', with: user.password
          click_button 'Update'
        end

        it { should have_css('#error_explanation') }
      end
    end
  end
end