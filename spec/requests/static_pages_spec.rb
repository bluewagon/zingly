require 'spec_helper'

describe 'Static Pages' do
  subject { page }

  describe 'Home Page' do
    before { visit root_path }

    it { should have_title('ArZing') }
    it { should have_css('form#new_user') }
    it { should have_css('form#login_user') }

    describe 'when logging in' do
      before do
        visit root_path
      end

      describe 'with invalid information' do
        it 'should display error when no information present' do
          click_button('Sign in')
          page.should have_text('Invalid')
        end
      end

      describe 'with valid information' do
        let(:user) { FactoryGirl.create(:user) }
        before do
          user.save!
          within('#login_user') do
            fill_in 'Email', with: user.email
            fill_in 'Password', with: user.password
            click_button 'Sign in'
          end
        end

        it { should have_title(user.email) }
      end
    end # 'when logging in'

    describe 'when registering' do
      let(:user) { FactoryGirl.create(:user) }
      before do
        visit root_path
      end

      describe 'with blank information' do
        before do
          within('#new_user') do
            click_button 'Sign up'
          end
        end

        it { should have_text("Email can't be blank") }
        it { should have_text("Password can't be blank") }
      end

      describe 'with an existing account' do
        
        before do
          user.save!
          within('#new_user') do
            fill_in 'Email', with: user.email
            fill_in 'Password', with: user.password
            fill_in 'Password confirmation', with: user.password
            click_button 'Sign up'
          end
        end

        it { should have_text('Email has already been taken') }
      end

      describe 'with non-matching passwords' do
        before do
          user.save!
          within('#new_user') do
            fill_in 'Email', with: user.email
            fill_in 'Password', with: user.password
            fill_in 'Password confirmation', with: 'foobar0983'
            click_button 'Sign up'
          end
        end

        it { should have_text("Password confirmation doesn't match Password") }
      end

      describe 'with correct data' do
        before do
          within('#new_user') do
            fill_in 'Email', with: 'test123@example.com'
            fill_in 'Password', with: user.password
            fill_in 'Password confirmation', with: user.password
            click_button 'Sign up'
          end
        end

        it { should have_title('test123@example.com') }
      end
    end # 'when registering'

  end
end
