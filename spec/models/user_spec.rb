# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  username               :string(255)
#

require 'spec_helper'

describe User do

  before do
    @user = User.new(email: 'test@example.com', password: 'foobar123', password_confirmation: 'foobar123', 
      username: 'bluewagon')
  end

  subject { @user }

  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:username) }
  it { should respond_to(:groups) }

  it { should be_valid }

  describe 'when username is nil' do
    before { @user.username = nil }
    it { should be_valid }
  end

  describe 'when username is just spaces' do
    before { @user.username = ' ' * 10 }
    it { should be_valid }

    it 'should be nil' do
      expect { @user.username == nil }
    end
  end

  describe 'when username is less than min characters' do
    before { @user.username = 'a' }
    it { should_not be_valid }
  end

  describe 'when username is over max limit' do
    before { @user.username = 'a' * 31 }
    it { should_not be_valid }
  end
end