# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  admin_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe Group do
  let(:user) { FactoryGirl.create(:user) }
  before do
    user.save!
    @group = Group.new(name: 'Test group', description: 'Group A: control group', admin: user)
    @group.users << user
  end

  subject { @group }

  it { should respond_to(:id) }
  it { should respond_to(:name) }
  it { should respond_to(:description) }
  it { should respond_to(:admin) }
  it { should respond_to(:users) }

  it { should be_valid }

  its(:admin) { should eq(user) }
  its(:users) { should include(user) }

  describe 'when name is missing' do
    before { @group.name = nil }
    it { should_not be_valid }
  end

  describe 'when description is missing' do
    before { @group.description = nil }
    it { should_not be_valid }
  end

  describe 'when admin is missing' do
    before { @group.admin = nil }
    it { should_not be_valid }
  end

  describe 'when there are no users' do
    before { @group.users.clear }
    it { should_not be_valid }
  end

  describe 'when name is less than min length' do
    before { @group.name = "aa" }
    it { should_not be_valid }
  end

  describe 'when name is longer than max length' do
    before { @group.name = "a" * 51 }
    it { should_not be_valid }
  end
end
