# == Schema Information
#
# Table name: zings
#
#  id              :integer          not null, primary key
#  zing_for_id     :integer
#  zing_against_id :integer
#  description     :string(255)
#  creator_id      :integer
#  group_id        :integer
#  created_at      :datetime
#  updated_at      :datetime
#

require 'spec_helper'

describe Zing do
  let(:user_one) { FactoryGirl.create(:user) }
  let(:user_two) { FactoryGirl.create(:user) }
  let(:user_three) { FactoryGirl.create(:user) }
  let(:user_four) { FactoryGirl.create(:user) }

  before do
    @group = Group.new(name: 'test', description: 'test', admin: user_one)
    @group.users << user_one
    @group.users << user_two
    @group.users << user_three
    @group.save!
    @zing = @group.zings.build(zing_for: user_one, zing_against: user_two, 
      creator: user_three, description: 'test')
  end

  subject { @zing }

  it { should be_valid }
  it { should respond_to(:zing_for) }
  it { should respond_to(:zing_against) }
  it { should respond_to(:creator) }
  it { should respond_to(:group_id) }
  it { should respond_to(:description) }

  describe 'when zing_for and zing_against match' do
    before do
      @zing.zing_for = user_one
      @zing.zing_against = user_one
    end

    it { should_not be_valid }
  end

  describe 'when zing_for is not in group' do
    before do
      @zing.zing_for = user_four
    end

    it { should_not be_valid }
  end

  describe 'when zing_against is not in group' do
    before do
      @zing.zing_against = user_four
    end

    it { should_not be_valid }
  end

  describe 'when creator is not in group' do
    before { @zing.creator = user_four }

    it { should_not be_valid }
  end

  describe 'when description is blank' do
    before { @zing.description = nil }

    it { should be_valid }
  end

end
