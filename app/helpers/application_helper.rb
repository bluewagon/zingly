module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def display_name(user)
  	current_user.username.blank? ? current_user.email : current_user.username 
  end
end
