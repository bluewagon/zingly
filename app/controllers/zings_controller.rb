class ZingsController < ApplicationController
  before_filter :authenticate_user!

  def new
    @zing = Zing.new
    @zing.group_id = params[:group_id]
    @zing.creator = current_user
    @group = Group.find(@zing.group_id)
  end

  def create
    @group = Group.find(params[:group_id])
    @zing = @group.zings.build(group_params)
    @zing.creator = current_user

    user_zing_id = params[:zing][:zing_for_id].to_i
    user_zinged_id = params[:zing][:zing_against_id].to_i

    user_zing_index = @group.rankings.index { |a| a.user_id == user_zing_id }
    user_zinged_index = @group.rankings.index { |a| a.user_id == user_zinged_id }

    user_zing = @group.rankings[user_zing_index]
    user_zinged = @group.rankings[user_zinged_index]

    user_zing.points += 1
    user_zing.zings_for += 1
    #user_zing.last_position = user_zing_index + 1

    user_zinged.points -= 1
    user_zinged.zings_against += 1
    #user_zinged.last_position = user_zinged_index + 1

    user_zing.save!
    user_zinged.save!

    set_last_position

    @group.save!

    if @zing.save!
      flash[:success] = 'Successfully added zing'
      redirect_to group_path(@group)
    else
      render 'new'
    end
  end

  def list
    @group = Group.find(params[:group_id])
    user_index = @group.users.index { |a| a.id == params[:id].to_i }
    user = @group.users[user_index]
    @zings = @group.zings.where('zing_for_id = ? OR zing_against_id = ?', user.id, user.id)
  end

  private
    def group_params
      params.require(:zing).permit(:zing_for_id, :zing_against_id, :creator, :description)
    end

    def set_last_position
      counter = 1
      @group.rankings.each do |user|
        user.last_position = counter
        counter += 1
      end
    end
end
