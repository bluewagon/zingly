class GroupsController < ApplicationController
  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_filter :admin_user, only: [:edit, :update, :destroy, :remove_user]
  before_filter :group_user, only: [:show, :leave]

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(group_params)
    @group.admin = current_user
    @group.users << current_user
    if @group.save
      flash[:success] = 'New Group Created!'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def join
    
  end

  def show
    @group = Group.find(params[:id])
  end

  def edit
    @group = Group.find(params[:id])
    @users = @group.users
  end

  def update
    if @group.update_attributes(group_params)
      flash[:success] = 'Group updated'
      redirect_to @group
    else
      render 'edit'
    end
  end

  def leave
    @group = Group.find(params[:id])
    if current_user == @group.admin
      flash[:non_form_error] = 'Unable to leave group while admin of the group'
      redirect_to @group
    else
      @group.users.delete(current_user)
      redirect_to @group
    end
  end

  def remove_user
    @group = Group.find(params[:id])
    user = @group.users.find(params[:user])

    if current_user == user
      flash[:non_form_error] = 'Cannot remove self while admin'
      redirect_to @group
    else
      @group.users.delete(user)
      flash[:success] = 'Removed user'
      redirect_to @group
    end
  end

  def destroy
    @group = Group.find(params[:id]).destroy
    redirect_to root_path
  end

  private
    def group_params
      params.require(:group).permit(:name, :description, :admin_id)
    end

    def admin_user
      @group = Group.find(params[:id])
      redirect_to group_path unless current_user == @group.admin
    end

    def group_user
      @group = Group.find(params[:id])
      redirect_to root_path unless @group.users.include?(current_user)
    end
end
