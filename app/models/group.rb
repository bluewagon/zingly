# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  admin_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Group < ActiveRecord::Base
  belongs_to :admin, class_name: 'User'
  has_many :rankings, -> { order('points DESC, zings_for DESC, user_id ASC') }, autosave: true
  has_many :users, -> { order('rankings.points DESC, zings_for DESC') }, through: :rankings
  has_many :zings

  #before_validation :strip_whitespace

  validates :name, presence: true, length: { maximum: 50, minimum: 3 }

  validates :description, presence: true

  validates :admin, presence: true

  validates :users, presence: true

  private
    def strip_whitespace
      self.name = self.name.strip
      self.description = self.description.strip
    end
end
