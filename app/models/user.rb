# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  username               :string(255)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #before_validation :strip_username

  has_many :rankings
  has_many :groups, through: :rankings
  has_many :zings

  validate :user_not_blank

  #validates :username, allow_nil: true, length: { maximum: 30, minimum: 3 }

  def get_zing_count(group_id)
    @group = Group.find(group_id)
    zings = @group.zings.where(zing_for_id: self.id)
    zings.count
  end

  def get_zinged_count(group_id)
    @group ||= Group.find(group_id)
    zingeds = @group.zings.where(zing_against_id: self.id)
    zingeds.count
  end

  def get_zing_difference(group_id)
    get_zing_count(group_id) - get_zinged_count(group_id)
  end

  private



    def strip_username
      if self.username
        self.username = self.username.strip
      end
    end

    def user_not_blank
      strip_username

      if self.username
        # if username.blank? 
        #   errors.add(:base, 'username cannot be blank')
        # end
        if self.username.blank?
          self.username = nil

        elsif self.username.length < 3
          errors.add(:base, 'username is too short')

        elsif self.username.length > 30
          errors.add(:base, 'username is too long')
        end
      end
    end
end
