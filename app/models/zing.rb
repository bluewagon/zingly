# == Schema Information
#
# Table name: zings
#
#  id              :integer          not null, primary key
#  zing_for_id     :integer
#  zing_against_id :integer
#  description     :string(255)
#  creator_id      :integer
#  group_id        :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Zing < ActiveRecord::Base

  belongs_to :zing_for, class_name: 'User'
  belongs_to :zing_against, class_name: 'User'
  belongs_to :creator, class_name: 'User'

  validates :zing_for, presence: true
  validates :zing_against, presence: true
  validates :creator, presence: true
  #validates :description, presence: true

  validate :validate_zings

  private

    def validate_zings
      group = Group.find(self.group_id)
      if self.zing_for == self.zing_against
        errors.add(:base, 'users cannot zing themselves')
      end

      unless group.users.include?(self.zing_for)
        errors.add(:base, 'user must belong to group')
      end

      unless group.users.include?(self.zing_against)
        errors.add(:base, 'user must belong to group')
      end

      unless group.users.include?(self.creator)
        errors.add(:base, 'user must belong to group')
      end
    end
end
