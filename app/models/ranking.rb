# == Schema Information
#
# Table name: rankings
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  group_id      :integer
#  points        :integer          default(0)
#  created_at    :datetime
#  updated_at    :datetime
#  zings_for     :integer          default(0)
#  zings_against :integer          default(0)
#

class Ranking < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  # TODO: what is the consequence of having zings_for and zings_against as DB columns and take up storage space
  #   compared to having the count of zings_for and zings_against called to DB and then subtracting in memory
end
