class AddZingToRanking < ActiveRecord::Migration
  def change
    add_column :rankings, :zings_for, :integer, default: 0
    add_column :rankings, :zings_against, :integer, default: 0
  end
end
