class CreateRankings < ActiveRecord::Migration
  def change
    #drop_table 'groups_users'

    create_table :rankings do |t|
      t.belongs_to :user
      t.belongs_to :group

      t.integer :points, default: 0

      t.timestamps
    end
  end
end
