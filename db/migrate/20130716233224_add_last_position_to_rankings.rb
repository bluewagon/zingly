class AddLastPositionToRankings < ActiveRecord::Migration
  def change
    add_column :rankings, :last_position, :integer
  end
end
