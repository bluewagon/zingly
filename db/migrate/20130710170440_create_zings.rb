class CreateZings < ActiveRecord::Migration
  def change
    create_table :zings do |t|
      t.integer :zing_for_id
      t.integer :zing_against_id
      t.string :description

      t.integer :creator_id
      #t.belongs_to :user
      t.belongs_to :group

      t.timestamps
    end
  end
end
