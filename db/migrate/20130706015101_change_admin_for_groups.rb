class ChangeAdminForGroups < ActiveRecord::Migration
  def change
    rename_column :groups, :admin, :admin_id
  end
end
